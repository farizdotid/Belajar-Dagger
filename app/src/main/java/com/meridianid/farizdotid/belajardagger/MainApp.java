package com.meridianid.farizdotid.belajardagger;

import android.app.Application;

/**
 * Created by fariz ramadhan.
 * website : www.farizdotid.com
 * github : https://github.com/farizdotid
 */
public class MainApp extends Application {

    DiComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        component = DaggerDiComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public DiComponent getComponent(){
        return component;
    }
}
