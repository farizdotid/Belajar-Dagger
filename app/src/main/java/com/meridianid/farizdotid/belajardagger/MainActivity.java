package com.meridianid.farizdotid.belajardagger;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {

    @Inject UserService userService;
    @Inject SharedPreferences sharedPref;

    TextView tvResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvResult = (TextView) findViewById(R.id.tvResult);
        ((MainApp) getApplication()).getComponent().inject(this);

        userService.addNewUser("1", "Fariz", "Ramadhan");
        sharedPref.edit().putString(userService.getUser().getId(),
                userService.getUser().toString()).commit();

        tvResult.setText("Hallo User " + sharedPref.getString(userService.getUser().getId(), ""));
    }
}
