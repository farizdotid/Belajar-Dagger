package com.meridianid.farizdotid.belajardagger;

import javax.inject.Singleton;

import dagger.Component;


/**
 * Created by fariz ramadhan.
 * website : www.farizdotid.com
 * github : https://github.com/farizdotid
 */

@Singleton
@Component(modules = {AppModule.class, UserModule.class})
public interface DiComponent {

    void inject(MainActivity activity);
}
