package com.meridianid.farizdotid.belajardagger;

/**
 * Created by fariz ramadhan.
 * website : www.farizdotid.com
 * github : https://github.com/farizdotid
 */
public class UserService {

    private User user;

    public UserService(User user){
        this.user = user;
    }

    public void addNewUser(String id, String firstName, String lastName){
        user.setId(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
    }

    public User getUser(){
        return user;
    }
}
