package com.meridianid.farizdotid.belajardagger;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by fariz ramadhan.
 * website : www.farizdotid.com
 * github : https://github.com/farizdotid
 */
@Module
public class UserModule {

    @Provides
    @Singleton
    public UserService provideUserService(){
        return new UserService(new User());
    }
}
